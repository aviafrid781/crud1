import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './componants/login/login.component';
import { AboutComponent } from './componants/about/about.component';
import { RegisterComponent } from './componants/register/register.component';
import { UpdateStudentsComponent } from './componants/update-students/update-students.component';
import { HomeComponent } from './componants/home/home.component';
import { UserListComponent } from './componants/user-list/user-list.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "about",
    component: AboutComponent
  },

  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "update/:id",
    component: UpdateStudentsComponent
  },
  {
    path: "userlist",
    component: UserListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
