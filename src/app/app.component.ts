import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular crud Practice';
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }
  logging() {
    return localStorage.getItem('email')
  }
  onlogout() {
    localStorage.removeItem('email')
  }
}
