import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/user.service'

@Component({
  selector: 'app-update-students',
  templateUrl: './update-students.component.html',
  styleUrls: ['./update-students.component.css']
})
export class UpdateStudentsComponent implements OnInit {
  constructor(private user: UserService, private route: ActivatedRoute) { }

  updateData = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  }
  );

  ngOnInit(): void {
    //console.log( this.route.snapshot.params['id'])
    this.user.getUserById(this.route.snapshot.params['id']).subscribe((res:any) => {
      // console.log(res);
      this.updateData = new FormGroup({
        name: new FormControl(res['name'], Validators.required),
        email: new FormControl(res['email'], Validators.required),
        password: new FormControl(res['password'], Validators.required)
      }
      );

    })
  }

  submitUpdateData() {
    this.user.updateUser(this.route.snapshot.params['id'],this.updateData.value ).subscribe((res) => {
      console.log(res);
      alert('upddate Successfully')
      this.updateData.reset({})
    })
   
  }

}
