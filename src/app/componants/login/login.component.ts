import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  //  constructor(private saveStudentData: AddStudentService) { }
  constructor(private formbuilder: FormBuilder, private http: HttpClient, private router: Router) { }
  public loginForm!: FormGroup;

  ngOnInit(): void {
    this.loginForm = this.formbuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }
  login() {
    this.http.get<any>('http://localhost:3000/user')
      .subscribe(res => {
        console.log(this.loginForm.value);
        const user = res.find((a: any) => {
          return a.email == this.loginForm.value.email && a.password == this.loginForm.value.password
        });
        if (user) {
          localStorage.setItem('email',user.email)
          alert('Login Successfully')
          this.loginForm.reset();
          this.router.navigate(['add'])
        } else {
          alert('User Not Valid');
          this.loginForm.reset();
        }
      });
  }



}
