import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public isloading: boolean = false;
  constructor(private user: UserService) { }

  studentData: any = [];
  ngOnInit(): void {
    this.isloading = true;
    this.user.getAllStudent().subscribe((data) => {
      this.studentData = data;
      setTimeout(() => this.isloading = false, 500)
      // this.isloading = false;
    }, err => {
      this.isloading = false;
    })
  }


  deletedStudent(userId: any) {
    // console.log(userId);
    this.user.DeletedStudent(userId).subscribe((res) => {
      //console.log(res)
      this.ngOnInit();
    })
  }


}
