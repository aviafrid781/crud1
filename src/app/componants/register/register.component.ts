import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../user.service'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private saveUserData: UserService) { }

  registerData = new FormGroup({
    name: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required)
  }
  );
  
  ngOnInit(): void {

  }

  saveData() {
    this.saveUserData.saveStudent(this.registerData.value).subscribe((res) => {
      alert('Registation Successfully')
      this.registerData.reset({})
    })
  }
  

}
