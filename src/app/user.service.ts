import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = 'http://localhost:3000/user';
  constructor(private http: HttpClient) { }

  getAllStudent() {
    return this.http.get(this.url);
  }
  saveStudent(data:any) {
   return this.http.post(this.url,data);
  }
  DeletedStudent(id:any) {
    return this.http.delete(`${this.url}/${id}`);
   }
   getUserById(id:any) {
    return this.http.get(`${this.url}/${id}`);
  }
  updateUser(id:any,data:any) {
    return this.http.put(`${this.url}/${id}`,data);
  }
}
